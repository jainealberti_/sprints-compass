# Descrição do projeto

Este é um projeto em desenvolvimento que contém todas as atividades diárias realizadas ao longo das 6 sprints quinzenais do programa de bolsas da Compass de Automação em Java.

O programa terá duração de 3 meses e a cada sprint terá um Challange diferente.

Na imagem abaixo é possível observar os conteúdos da jornadada as sprints.

![Cronograma](img/sprints.png)