# Dia 1 - Ágil (22/05/2023)

# Onboard

## Matriz de Eisenhower

Ferramenta utilizada para priorizar tarefas diárias e melhorar a produtividade. 

É utilizada para categorizar as atividades que precisam ser feitas de acordo com 2 critérios: urgência e importância. 

Tarefas importantes são aquelas que estão diretamente relacionadas ao alcance de determinada meta ou objetivo. 

Já tarefas urgentes são diretamente ligadas ao prazo estabelecido para sua realização e/ou suas consequências para a empresa.

![MatrizEisenhower](img/eisenhower.jpg)

## Miro

Ferramenta sugerida para organização e gerenciamento as tarefas que serão realizadas durante o programa de bolsas.

## Git e GitHub

**Git**: é uma ferramenta de controle de versionamento de arquivos, muito utilizada principalmente na área de desenvolvimento de software.

**GitLab**: é uma plataforma usada para gerenciar os códigos e criar um ambiente de colaboração entre desenvolvedora, utilizando o Git como sistema de controle.

Foi efetuado o download do Git no computador pessoal, e também a criação de conta/usuário no GitLab.

Na sequência foi criado um arquivo Readme das atividade das Sprints no computador e posteriormente feito o versionamento para o GitLab via comandos.

Os comandos utilizados foram:

| **Comando**   | **Descrição**  |
|--------|--------|
| **git config --global user.name**   | Utilizado para definir meu nome de usuário   |
| **git config --global user.email**   | Utilizado para associar meu git com gitlab   |
| **git init**   | Utilizado para inicializar o repositório   |
| **git add**   | Comando que adiciona o arquivo Readme para a staging area que fica aguardando um commit   |
| **git status**   | Usado para conferir quais arquivos estão prontos para serem comitados   |
| **git commit -m "criação do projeto"**   | Criado um commit com a mensagem de confirmação "criação do projeto"   |
| **git remote add origin**   | Adicinado o repositório local para meu repositório no GitLab  |
| **git push origin main**   | Comando usado para enviar o commit local para o repositório main remoto   |

Na sequencia foi criado um novo arquivo Sprint 1 e realizado os comandos acima novamente.

## Readme

Um arquivo Readme é um documento importante em um repositório, pois fornece informações e instruções sobre o projeto. 

A sigla README significa Read Me,ou seja, leia-me, e é indicado exatamente para isso, nele contém informações relevantes que devem seremlidas pelos usuários que acessam o mesmo.

O Readme é escrito em formato de texto simples como Markdown e tem como extensão md.

## Resumo

Em resumo nesse dia 1 foi estudado sobre priorização de tarefas, ferramentas de auxilio de controle das tarefas, como criar um repositório no Git e GitLab, e também a criação do arquivo Readme.