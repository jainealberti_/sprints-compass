# Dia 2 – Ágil (23/05/2023)

# Planning Sprint 1

# Comunicação em equipe
A comunicação é fundamental em qualquer área, pois a mesma garante uma melhor compreensão entre os envolvidos, reduz retrabalhos e aumenta a produtivdade, ajuda a reduzir conflitos e construir relacionamentos saudáveis, ajuda em tomadas de decisão, torna o relacionamento da equipe mais forte.

# Scrum

Framework simples para gerenciar projetos complexos

## 3 pilares principais do Scrum:
- Transparência dos processos, requisitos de entrega e status;
- Inspeção constante de tudo o que está sendo feito (nas dailys ou nas Sprints);
- Adaptação, tanto do processo, quanto do produto ás mudanças;

# Práticas fundamentais:
- Papéis
- Eventos
- Artefatos

## Papéis fundamentais:
- **Product Owner** - desempenha um papel de facilitador e defensor da equipe de desenvolvimento, garantindo que o processo Scrum seja seguido adequadamente e que a equipe tenha todas as condições necessárias para realizar seu trabalho de forma eficiente e eficaz.
- **Scrum Master** - é responsável por representar os interesses dos stakeholders e definir a visão do produto. Suas principais responsabilidades incluem: definição de requisitos, gerenciamento de backlog, comunicação com stakeholders, tomada de decisão, aceitação do trabalho concluído.
- **Dev Team** - é responsável pela criação, desenvolvimento e entrega do produto ou projeto.
Sem esses 3 papéis não há Scrum.

## Eventos Básicos:
- **Sprint Planning** – Reunião de planejamento das Sprints, é na Sprint Planning onde se cria o Product Backlog.
- **Execução da Sprint** – períodos fixos em que alguns itens selecionados do Product Backlog serão selecionados e entregues. Duração geralmente de 2 a 4 semanas.
- **Daily Scrum** – Reuniões diárias onde todos do time participam e precisam responder 3 perguntas básicas: O que fez ontem? O que vai fazer hoje? Tem algum impedimento?
- **Revisão do Sprint** – realizado após a sprint, verificar se o que está sendo feito está de acordo com o esperado. É neste momento em que o Product Backlog é atualizado.
- **Retrospectiva da Sprint** – verifica a necessidade de adaptações no projeto, pontos positivos e negativos.

## Artefatos gerados:
- **Product Backlog** – lista de funcionalidades, requisitos, melhorias ou correções que precisam ser feitas em um produto. É mantido e gerenciado pelo PO.
- **Sprint Backlog** - lista de atividades que serão entregues em uma sprint.
- **Incremento/Entrega** - equipe apresenta os incrementos e entrega do progresso do produto.

Ferramentas sugeridas: Burdown Chart, Kanban Board.

![Scrum](img/scrum.jpg)

# Manifesto Ágil

É um documento que baseia-se em 4 valores e 12 princípios para o desenvolvimeto ágil de software.

**Os 4 valores do manifesto ágil são:**
1. Indivíduos e interações acima de processos e ferramentas
2. Software funcionando é melhor que documentação abrangente
3. Colaboração com o cliente acima de negociação de contratos
4. Responder a mudanças ao invés de seguir um plano

**Os 12 princípios do manifesto ágil são:**
1. Satisfação do cliente
2. Mudança em favor da vantagem competitiva
3. Prazos curtos
4. Trabalho em conjunto
5. Ambientação e suporte
6. Falar na cara
7. Funcionalidade
8. Ambiente de sustentabilidade
9. Padrões altos de tecnologia e design
10. Simplicidade
11. Autonomia
12. Reflexões para otimizações
