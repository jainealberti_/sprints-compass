# Dia 3 – Scrum, Papéis e responsabilidades (24/05/2023)

# Cultura Ágil/Scrum

## Objetivo 

O objetivo do Scrum é fazer pequenas entregas a cada 2 ou 4 semanas onde é possível o cliente ir validando essas entregas que agregam valor ao produto.

Ser ágil não é ser rápido, ser ágil é a cada sprint entregar uma “fatia” do projeto. Essas fatias precisam ser incrementais, ou seja, entregar algo que tenha valor e que o cliente já possa ir usando, não entregar algo sem sentido.

## Passo-a-passo do framework

1.	Necessidade do cliente é passada para o PO
2.	PO cria a user story
3.	PO constrói o backlog
4.	Planejamento da Sprint
5.	Sprint backlog
6.	Daily
7.	Review da Sprint
8.	Retrô

# O QA dentro de um time ágil

O principal objetivo de um QA é garantir a qualidade na entrega de um produto final. Além disso, o QA pode estar presente em todas as etapas do projeto, garantindo que boas práticas de qualidade estão sendo aplicadas.
Ter um QA no time tem um impacto positivo, pois os benefícios podem incluir: entregas com maior valor agregado, otimização do tempo, menor risco de incidentes críticos na reta final do produto, entre outrso.

# Epic, Feature and Story

São práticas de criação e organização do backlog do produto.
- **User Stories (Histórias do usuário)**: são as representações clara e informal que descrevem uma funcionalidade ou requisito do ponto de vista do usuário. 
- **Feature (Funcionalidade ou característica)**: as features são um agrupamento de histórias do usuário. Ela expressa uma funcionalidade do produto que contém os requisitos funcionais e suas regras e exceções.
- **Epic (Épico)**: são as grandes histórias dos usuários, descrevem as funcionalidades e requisitos. Representam objetivos de longo prazo e são desmembrados em Features e User Stories mais detalhadas.
