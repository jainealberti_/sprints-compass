# Challange em grupo - Grupo 3 (26/05/2023)

# FUNDAMENTOS DO TESTE

## 1.  Como Gerar Qualidade

Gerar qualidade não é apenas testar, o principal é objetivo é que solicitação do cliente possa ser atendida.<br>
Saber gerir os testes não é apenas testar e relatar, mas sim entender os motivos, causas e objetivos em torno deles.

TESTE DINÂMICO X TESTE ESTÁTICO<br>
Discutimos a diferença entre teste dinâmico e estático, enquanto o teste dinâmico é aquele que envolve a real execução do software, o estático envolve análise do código e documentação sem a execução do software.

TESTES E DEPURAÇÃO<br>
O teste quando executado pode apresentar falhas causadas por defeitos no software. Já os testes de confirmação são aplicados com a finalidade de conferir se os defeitos foram corrigidos. Ambos são mais de responsabilidade do QA. Já a depuração geralmente fica sob responsabilidade do desenvolvedor pois é realizada a localização e correção dos defeitos.

EFICIÊNCIA X EFICÁCIA<br>
O objetivo é ser eficiente, ou seja, fazer as tarefas de maneira correta, e ser eficaz, ou seja, que o resultado seja desejável.

ERRO, DEFEITO E FALHA<br>
Como tudo em um sistema precisa estar conectado de alguma maneira, um erro, apesar de não ser o mais catastrófico, entre defeito e falha, ele acaba sendo uma porta de entrada para um efeito cascata que leva, defeitos e falhas, e consequentemente a um programa inutilizado.

## 2. Os Sete Princípios do Teste
Seguindo os sete princípios de testes, é possível  atingir eficiência acima da eficácia, pois uma cobertura extensa tem menos valor que uma cobertura que uma cobertura eficaz porém mais concisa e de menor custo.

## 3.  Fatores contextuais
Dentro do ciclo de vida de um projeto, é de suma importância que um QA saiba os detalhes e minúcias desde os requisitos que o projeto aspira, tanto quando como e onde ele será utilizado, assim sendo de sua responsabilidade compreender e assegurar que em todas as faces que seja aplicado, a qualidade desejada seja atingida.

## 4.  Atividades e Tarefas de teste
O QA não apenas aplica os testes, imaginamos como será nosso dia a dia e vemos como é importante a participação e envolvimento em todo o desenvolvimento e planejamento de como será desenvolvido e aplicado esse planejamento. Assim também gerando documentos e metodologias eficazes que irão dinamizar o processo de desenvolvimento dos testes e coleta de métricas.

## 5. A Psicologia do Teste
Foi debatido como as relações entre QAs e Devs acabam sendo ásperas, como também foi trazido, vivências sobre isso. Chegamos a conclusão que o respeito é a chave e que erros são uma oportunidade de aprendizado

## Autores
- Higor Milani
- Jaíne Alberti
- Matheus Casagrande
