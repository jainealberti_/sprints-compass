# Dia 4 - MasterClass (25/05/2023)

# Fundamentos do teste de software

# 1.	Como gerar Qualidade
É possível gerar qualidade em um produto através de testes. Mas gerar qualidade não é somente testar, o principal objetivo deve ser garantir que realmente a solicitação do cliente está sendo atendida.
Objetivos típicos do teste:

![Qualidade](img/qualidade.jpg)

## Teste Dinâmico x Teste Estático<br>
**Teste Dinâmico**: é o tipo de teste que envolve a execução real do software.<br>
**Teste Estático**: envolve análise do código e documentação sem executar o software.

## Teste e Depuração<br>
**Teste**: quando o teste é executado pode apresentar as falhas causadas por defeitos do software.<br>
**Depuração**: é a atividade em que é realizada a localização e correção dos defeitos apresentados no teste.<br>
**Testes de confirmação**: são executados com a finalidade de conferir se os defeitos foram corrigidos.<br>
É mais comum que o testador foque no teste e teste de confirmação, enquanto a tarefa de depuração fica de responsabilidade do desenvolvedor.

## TESTE = QUALIDADE
Um teste é capaz de aumentar a qualidade de um software quando os defeitos são encontrados e corrigidos, sistema entrega o que foi solicitado pelo cliente, requisitos como confiabilidade, usabilidade entre outros são verificados.<br>
Qualidade custa menos pois reduz retrabalhos, custos.<br>
Mais qualidade = Produção mais rápida = Mais produtividade.

## Erro, Defeito e Falha
É importante saber a diferença entre cada um.<br>
![Erro](img/erro.jpg) **Erro**: Ação humana que produz um resultado incorreto.<br>
![Defeito](img/defeito.jpg) **Defeito**: Uma imperfeição ou deficiência em um produto de trabalho que faz com que o mesmo não atenda aos requisitos.<br>
![Falha](img/falha.jpg) **Falha**: Um evento no qual um componente ou sistema não executa uma função necessária dentro dos limites especificados.

# 2.	Os Sete Princípios do Teste
**1.  O teste mostra a presença de defeitos**: testar não é provar que o software está correto, mesmo que não se encontre erros não significa que os mesmos não existam. O teste mostra a ausência de defeitos, mas nunca a falta deles.<br>
**2. Testes exaustivos são impossíveis**: testar todas as entradas e saídas de um software é praticamente impossível.<br>
**3. O teste inicial economiza tempo e dinheiro**: trazer os testes para as fases mais iniciais do desenvolvimento. Quanto mais cedo é pego um defeito, mas barato ele é para corrigir.<br>
**4. Defeitos se agrupam**: onde a gente encontra um erro existe uma grande probabilidade de encontrar outros defeitos. Princípio de Pareto (80-20) 80% dos defeitos está em 20% das linhas de código.<br>
**5. Cuidado com o paradoxo do pesticida**: testes precisam ser atualizados regularmente caso contrário se tornam ineficazes (igual um pesticida quando aplicado muitas vezes na mesma praga). Dar manutenção e evoluir os testes.<br>
**6. O teste depende do contexto**: testes realizados para um sistema bancário serão diferentes para um jogo de celular por exemplo. Cada lugar que for realizar os testes será diferente, com funcionalidades e requisitos diferentes.<br> 
**7. Ausência de erros é uma ilusão**: se você não encontrou erros nos seus testes provavelmente seus testes não estão bons o suficiente. Independente de quantos defeitos foram encontrados e corrigidos.

# 3.	Fatores contextuais
O QA em um processo de desenvolvimento de software precisa ter um conhecimento e participação abrangente, desde fase do levantamento de requisitos, qual a finalidade do mesmo, e assegurar que o mesmo cumpra com as funcionalidades desejada.<br>
Fatores contextuais que influenciam o processo de teste:

![FatoresContextuais](img/fatores_contextuais.jpg)

# 4.	Atividades de Teste
Para que os testes sejam melhor otimizados o QA necessita não só aplicar o teste, é preciso que haja um planejamento, uma análise, uma modelagem, a implementação, a execução e a conclusão do teste. Além disso, é necessário sempre haver um monitoramento e controle do teste.
- **Planejamento do teste**: essa é a etapa inicial do teste, é nela em que ocorre a definição dos propósitos e como será a abordagem do teste. Além disso é feito a criação do plano de teste, definição do escopo, seleção das métricas e determinação do nível de detalhamento do mesmo.
- **Análise do teste**: baseado nas condições anteriores é executado a análise do teste.
- **Modelagem do teste**: após feito o planejamento e a análise do teste nessa etapa definem-se como a modelagem será executada, quais os objetivos, quais serão as entradas e os resultados esperados dessas entradas, sob quais condições, entre outros aspectos.
- **Implementação do teste**: na fase de implementação do teste é criado os scripts, as suítes, preparam-se os dados que serão utilizados no teste. Além disso nessa fase questiona-se: “Agora temos tudo para executar os testes?”.
- **Execução do teste**: nesta fase é que efetivamente os testes são executados de acordo com o cronograma elaborado anteriormente. Durante a execução do teste é preciso que o QA além de executar colha as evidências, compara os resultados com o que estava planejado, analisa possíveis defeitos e as prováveis causas, comunica para o restante da equipe quais são os defeitos, registra os resultados obtidos na execução do teste, e na sequencia executa o teste de confirmação e regressão.
- **Conclusão do teste**: e por fim, após a execução e correções no software é feita a conclusão do teste. Analisa-se se todos as atividades planejadas foram executadas, corrigidas, evidenciadas, e também verificado pontos a serem melhorados e aprimorados em testes futuros.

# 5.	A psicologia do teste

![Psicologia](img/psicologia.jpg)

A relação entre QA e desenvolvedores pode ser por muitas vezes um pouco conturbada. Para que essa relação seja sempre amistosa é preciso haver respeito e comunicação entre ambas as partes para que o objetivo principal que é entregar um software funcional e com a menor quantidade de erros seja atendido.

![Psicologia,2](img/psicologia2.jpg)

