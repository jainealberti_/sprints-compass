# Dia 5 – MasterClass (26/05/2023)

# Fundamentos do teste de software (Back-End)

## Ciclo de vida dos testes
O ciclo de vida do teste anda alinhado com o ciclo de desenvolvimento, conforme o desenvolvimento avança os testes avançam também. Antecipar os testes é muito benéfico para o projeto, pois quanto mais cedo começar a ser testado e encontrado os bugs, mais cedo também começa a ser feito as correções.

![Teste](img/ciclo_teste.jpg)

# Pirâmide de testes
A principal função da pirâmide de testes é dar uma orientação em relação aos níveis e quantidades de testes a serem realizados. Basicamente ela divide-se em 3 partes: na base os testes unitários, no meio os testes de integração e mais no topo os testes E2E. Quanto mais na base da pirâmide mais o teste é fácil de fazer e rápido de rodar, enquanto mais no topo é mais difícil e lento.

![Piramide](img/piramide.jpg)

## Teste unitário
- Servem para verificar se uma parte específica do código está funcionando corretamente, ou seja, a menor unidade de código é testada.
- Em ambiente de linguagem orientada a objetos a menor parte do código pode ser um método de uma classe.
- Geralmente os testes unitários são pequenos, simples e rápidos de rodar.
- Uma vantagem desse tipo de teste é que quando o mesmo falha você saberá exatamente onde está o problema.

## Teste de integração
- São testes realizados com o objetivo de verificar o funcionamento de algumas unidades em conjunto, pois pode ter sido testado duas unidades separadamente, porém a partir do momento em que eles interagem pode haver problemas. 
- Testam as funcionalidades, e não o sistema todo. 
- Eles são mais complicados e demorados que os testes unitários, mas bem mais simples e rápidos que os testes E2E.

## Teste E2E (end to end ou de ponta-a-ponta)
- Testes que simulam o ambiente real, e buscam verificar o comportamento do sistema como um todo “de uma ponta a outra”. 
- Geralmente é simulado a atividade que o usuário final teria, abre o navegador, preenche formulários, clica nos botões e, por fim, verifica se aconteceu o que era esperado. 
- Essas ações acontecem em um ambiente que não seja o de produção e quem executa elas são um robô. 
- É nesse teste que não testadas as funcionalidades das APIs. 
- Esse tipo de teste é mais complexo de escrever e costumam demorar um tempo considerável para rodar, além de que se apresentarem falhas é mais difícil descobrir onde está o problema, já que ele é bastante abrangente.
