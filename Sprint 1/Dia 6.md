# Dia 6 – MasterClass (29/05/2023)

# Regra 10 Myers
- Myers diz que *“testar é analisar um programa com a intenção de descobrir erros e defeitos”*.
- Na regra 10 de Myers diz que o custo da correção de um defeito tende a aumentar quanto mais tarde ele for encontrado. Quando corrigido uma falha em um software que já se encontra em produção estima-se que seu custo seja 100 ml vezes mais que se a mesma fosse corrigida na fase de levantamento de requisitos.

![Myers](img/myers.jpg)

# Princípio de Pareto
- No princípio de Pareto diz que 80% dos problemas está relacionado com 20% das causas. Quando é focado em 20% das causas 80% dos problemas podem ser resolvidos.

![Pareto](img/pareto.jpg)

# Teste de software

## Fundamentos do Teste de Software

- Teste em muitas vezes é visto como uma atividade destrutiva, mas na verdade ela é construtiva, visto que o objetivo deve ser o mesmo: entregar um produto funcional e com qualidade.
- Dentre os objetivos do teste estão: reduzir erros, minimizar riscos, atender as necessidades do cliente, maior satisfação ao cliente.
- Diversos são os benefícios em se realizar testes, podemos citar alguns como: maior segurança por parte dos clientes, melhoria na qualidade dos softwares, redução de gastos com correção de bugs.

## Verificação e Validação

São muito importantes dentro do escopo dos testes. A validação é a avaliação da veracidade do produto baseado nas necessidades informadas pelo cliente. Já a verificação avalia se o produto foi desenvolvido de conforme o previsto.

![Processo_em_v](img/processo_em_v.jpg)


## Testes Unitários

Como já visto antes, o teste unitário tem por objetivo testar a menor parte testável do sistema (unidade).

- TDD é uma técnica onde os testes são escritos primeiro e depois é desenvolvido o código de possa valida-lo. Entre as vantagens de usar esta abordagem estão: melhor qualidade no código já que os desenvolvedores precisam escrever seus códigos validando cada parte e garantindo assim que o código seja livre de erros. Também é vantagem pois gera maior confiabilidade, melhor manutenção do código, entre outras.

## Testes Funcionais

São testes que avaliam o funcionamento da aplicação (está construindo o produto certo?) considerando o comportamento externo do software. Dados de entrada são fornecidos, o teste é executado e o resultado obtido é comparado a um resultado esperado.

Outros tipos de testes incluem: 

    - Não Funcional (valida aspectos não funcionais como: desempenho, carga, stress, usabilidade, etc.); 
    - Estrutural (valida aspectos estruturais do software como: cobertura do código, decisão, sentença); 
    - Teste de Confirmação (valida remoção de defeitos); 
    - Teste de Regressão (executado quando ocorre alteração no software, verifica se não foram gerados novos defeitos)
    - Teste caixa preta: é uma técnica de teste de software que se concentra na validação do comportamento do sistema sem considerar a estrutura interna do código. Ele é baseado nas especificações do sistema, mas não requer conhecimento detalhado de sua implementação.
    - Teste de caixa branca, também conhecido como teste estrutural, é uma técnica de teste de software que se concentra na análise da estrutura interna do código-fonte. Ele envolve a compreensão do funcionamento interno do sistema, examinando o fluxo de controle, estruturas de dados, caminhos de execução e condições lógicas.
