# Dias 7 e 8 - Java (30/05/2023 e 31/05/2023)

![Java](img/java.jpg)

# Linguagem

Java é uma linguagem de programação orientada a objetos de distribuição gratuita, capaz de produzir softwares robustos e multiplataforma, que podem rodar em diversos tipos de microcomputadores, servidores e dispositivos de pequeno porte. Foi criada em 1995 pela Sun Microsystems e adquirida pela Oracle em 2009.

# Principais características

- **Multiplataforma**: programas desenvolvidos em Java podem ser executados em diferentes sistemas operacionais. Ex: um código é compilado em Windows, mas pode ser executado além do Windows em MacOs, Linux, entre outros.

- **Orientação a objetos**: tudo em Java é objeto, então permite a implementação de conceitos fundamentais da orientação a objetos, como encapsulamento, herança e polimorfismo.

- **Segurança**: pela JVM executar ocódigo em um ambiente sandbox acaba restringindo o acesso não autorizado aos resursos do sistema.

- **Biblioteca padrão robusta**: possui uma biblioteca padrão extensa e robusta, conhecida como Java API (Application Programming Interface).

- **Comunidade ampla**: possui uma comunidade grande de desenvolvedores em todo o mundo, o que possibilita em uma ampla quantidade de materiais disponíveis.


# Conceitos

- **Classes**: definem um modelo para criação de objetos (reais ou abstratos). É na classe onde se agrupa 2 tipos de elementos: atributos e métodos.

- **Objetos**: é a representação computacional de um elemento ou processo do mundo real. O objeto possui características ou propriedades que o descrevem, além disso possui um comportamento, que represneta uma ação ou resposta.

- **Método**: é a implementação de uma operação possível de ser realizada pelo objeto. Quando uma operação é invocada em um objeto, um método é executado para realizar a operação. Os métodos podem ter diferentes visibilidades: public, private, protected e default.


# Princípios

- **Encapsulamento**: é proteger membros de outra classe de acesso externo, permitindo somente sua manipulação de forma indireta (para tanto, utiliza a visibilidade).

- **Polimorfismo**: é a capacidade de um objeto ser referenciado de várias formas, permitindo que um único tipo seja tratado como diferentes tipos em contextos diferentes. Existem 2 tipos de polimorfismo: de sobrecarga e de substituição.

- **Herança**: herança tem o princípio da reutilização de código. Permite a reutilização de classes já existentes como instâncias de novas classes.

# Tipos de dados

O Java possui vários tipos de dados usados para declarar variáveis e métodos. Alguns deles são:

**Tipos Primitivos**

| Tipo        | Descrição | Exemplo |
|-------------|-------------|-------------|
| int         | Representa um número inteiro      | int idade = 3;          |
| float       | Representa um número com ponto flutuante      | float sal = 1825.54f;           |
| char        | Representa um caractere     | char letra = ‘G’;           |
| boolean     | Representa um valor verdadeiro ou falso      | boolean casado = false;           |

**Tipos de dados de referência**

| Tipo        | Descrição |
|-------------|-------------|
| String         | Representa uma sequência de caracteres      |
| Arrays       | Representa uma coleção de elementos do mesmo tipo de dados      |

**Tipos de dados numéricos**

| Tipo        | Descrição |
|-------------|-------------|
| Integer         | Representa valores inteiros e fornece métodos para manipulação de números inteiros      |
| Double       | Representa valores decimais e fornece métodos para manipulação de números decimais      |

# Estrutura de código

Abaixo a estrutura básica de programa Java com método executável:


        package olamundo;
    public class OlaMundo { // classe
        public static void main(String[] args) { // método principal
            System.out.println("Ola mundo!"); // saída
        }
    }





