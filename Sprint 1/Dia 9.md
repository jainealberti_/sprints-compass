# Dia 9 – MasterClass (01/05/2023)

# CyberSecurity

# 1. Como ser hackeado e perder todas suas informações

É muito fácil ser hackeado atualmente. O Phishing, que são e-mails com conteúdo duvidoso que tem por objetivo tentar fraudar para obter de forma ilegal informações como senhas bancárias, número de cartão de crédito, número de cpf e identidade, entre outras, aumentou em mais de 90% nos últimos anos. Os alvos na maioria são as pessoas e não só os sistemas, porque é muito mais fácil e rápido atacar pessoas do que sistemas. Algumas ações podem ser tomadas a fim de evitar ser hackeado:
- Evitar clicar em links contaminados/suspeitos
- Não conectar-se em redes wi-fi desconhecidas
- Procurar usar senhas diferentes para cada login

# 2. Novo OWASP TOP 10 – 2021

## O que é?

O Open Web Application Security Project, ou OWASP, é uma organização internacional open source dedicada a segurança de aplicativos web. Qualquer pessoa pode acessar seus materiais gratuitamente, fazendo com que qualquer um possa melhorar a segurança de seus próprios aplicativos web.

## OWASP Top 10

É um relatório elaborado por especialistas que apresenta e descreve os 10 riscos mais críticos em relação a segurança de aplicativos web.
A seguir as principais mudanças entre a OWASP Top 10 2017 x OWASP Top 10 2021:

![owasp](img/owasp.jpg) 

## OWASP API Security

Em paralelo a OWASP Top 10 existe uma lista semelhante direcionada para APIs, e chama-se OWASP API Security.  Ela lista as 10 maiores preocupações de segurança em aplicativos web, mas direcionado ao uso de APIs. 
 
# 3. Segurança de rede Wifi Doméstica

A rede doméstica pode trazer grandes riscos e uma ameaça silenciosa pois é difícil suspeitar que tem algo errado com nosso wifi.

As ameaças podem ser: sequestro de DNS, Botnets e Proxy, monitoramento e tráfego, vazamento de dados pessoais e senhas, entre outros.

Para se precaver e defender dessas ameaças dentro da interface do roteador podemos:
1. Alterar as informações padrão do roteador
2. Utilizar WPA2 e AES256 (ferramentas que criptografam o tráfego)
3. Alterar periodicamente o nome da rede e senha
4. Ativar o firewall do roteador pessoal
5. Bloquear dispositivos desconhecidos
6. Ocultar o nome da rede
7. Atualizar o firmware dos equipamentos 

# 4. Segurança Digital - Dicas para se proteger o dia a dia

1.	Ter boas senhas: criar senhas longas com mais de 12 caracteres, mas que você irá memorizar, incluir caracteres especiais, números, letras maiúsculas e minúsculas.
2.	Usar um gerenciador de senhas: não usar a mesma senha em mais de um serviço, e para lembrar de todas essas senhas criadas use um gerenciador para te auxiliar.
3.	Múltiplo fator de autenticação: habilitar essa função. Ela utilizada diversas tecnologias para autenticar a identidade do usuário.
4.	Cuidado com Phishing: são mensagens não solicitadas ou spam com características de fraude e que geralmente vem por e-mail, mas também podem vir por whats, SMS etc.
5.	Antivírus: importante é ter um antivírus, seja gratuito ou pago.
6.	Manter o software atualizado.
7.	Fazer backup de tudo.
8.	Segurança no navegador.
9.	Privacidade: ter cuidado com seus dados colocados na internet, ter a premissa que se os dados estão na internet podem “vazar”.
10.	Transações na internet: utilizar sites de ecommerce que sejam confiáveis.

Todas essas dicais se auto complementam e ajudam a se proteger cada vez mais.

