
package appmedias;

import java.util.ArrayList;
import java.util.Scanner;

public class AppMedias {

    // uma array para armazenar em memória as avaliações
    static ArrayList<Pessoa> lista = new ArrayList<>();

    public static void main(String[] args) {
        lerAvaliacao();
        lerAvaliacao();
        lerAvaliacao();
        lerAvaliacao();
        relatorio();

    }

    private static void lerAvaliacao() {
        Pessoa objeto = new Pessoa();
        Scanner teclado = new Scanner(System.in);
        System.out.print("Matricula: ");
        objeto.matricula = teclado.nextInt();
        System.out.print("Nome: ");
        objeto.nomeAluno = teclado.next();
        System.out.print("Nota 1: ");
        objeto.nota1 = teclado.nextFloat();
        System.out.print("Nota 2: ");
        objeto.nota2 = teclado.nextFloat();

        System.out.println("Media: " + objeto.getMedia());
        System.out.println("Situacao: " + objeto.getSituacao());

        // armazenar na lista
        lista.add(objeto);

    }

    private static void relatorio() {
        for (Pessoa p : lista) {
            System.out.println(
                    p.matricula + " - "
                    + p.nomeAluno + " - "
                    + p.getMedia() + " - "
                    + p.getSituacao()
            );
        }
    }

}
