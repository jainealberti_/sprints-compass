
package appmedias;

public class Pessoa {
    String nomeAluno;
    Integer matricula;
    Float nota1;
    Float nota2;
    
    Float getMedia(){
        // Isto é uma variável local do método e não atributo!
        Float media = (nota1+nota2) / 2;
        return media;
    }
    
    String getSituacao(){
        Float media = getMedia();
        if (media >= 7.0){
            return "Aprovado";
        }else{
            return "Em exame";
        }
    }
    
}
