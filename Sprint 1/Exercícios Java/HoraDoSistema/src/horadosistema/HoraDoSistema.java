package horadosistema;

import java.util.Date;

public class HoraDoSistema {

    public static void main(String[] args) {
        Date relogio = new Date(); //relogio é um objeto porque tem o new
        System.out.println("A hora do sistema eh ");
        System.out.println(relogio.toString());
    }
    
}

// Esse código mostra a data e hora atual
